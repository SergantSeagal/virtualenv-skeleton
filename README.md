Virtual Environment Skeleton Project
------------------------------------
Makefile project to bootstrap an isolated virtualenv project.

Usage
-----
Add required packages to .pythonpackages
To install run:
```shell
make install
```
To cleanup the virtualenvironment use:
```shell
make cleanenv
```
